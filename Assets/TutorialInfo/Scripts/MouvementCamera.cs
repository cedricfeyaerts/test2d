﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouvementCamera : MonoBehaviour {

    public Transform player;
    private Transform tr;

	// Use this for initialization
	void Start () {
        tr = GetComponent<Transform>();
	}
	
	// Update is called once per frame
	void LateUpdate () {
        tr.position = new Vector3(player.position.x, player.position.y, tr.position.z);
	}
}
