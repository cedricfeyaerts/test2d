﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player_controller : MonoBehaviour {

    private Rigidbody2D rb;
    public float speed;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal") * speed;
        float moveVertical = Input.GetAxis("Vertical") * speed;
        Vector2 force = new Vector2(moveHorizontal, moveVertical);
        rb.AddForce(force);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        print("toto");
        print(collision.gameObject.name);
    }

    
}
